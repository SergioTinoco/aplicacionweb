/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SergioTinoco
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Hermanos {

    private List<Hermano> listaHermanos;

    public Hermanos() {
        listaHermanos = new ArrayList();
    }

    public List<Hermano> getListaHermanos() {
        return listaHermanos;
    }

    public void setListaHermanos(List<Hermano> listaHermanos) {
        this.listaHermanos = listaHermanos;
    }

}
