/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author 1Daw02
 */
@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {

    public static final byte PETICION_GET = 0;    // Seleccionar
    public static final byte PETICION_POST = 1;   // Insertar
    public static final byte PETICION_PUT = 2;    // Actualizar
    public static final byte PETICION_DELETE = 3; // Borrar

    private byte tipoPeticion;
    private static final Logger LOG = Logger.getLogger(Servlet.class.getName());
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Hermano persona = new Hermano();

    // declaro una lista de personas
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, PropertyException, JAXBException {
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet Servlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet Servlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
            try {
                EntityManager entityManager = Persistence.createEntityManagerFactory("AplicacionWebPU").createEntityManager();

                Query query = (Query) entityManager.createNamedQuery("Hermano.findAll");
                Hermanos hermanos = new Hermanos();
                hermanos.setListaHermanos(query.getResultList());
            // guardamos en un array de hermanos, los hermanos obtetidos de el objeto query

            // declaro un objeto hermanos, la listaHermanos de la clase hermanos esta vacia
                // paso la lista de hermanos a un objeto de hermanos para que no de error el marshal
                JAXBContext jaxbContext = JAXBContext.newInstance(Hermanos.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                //generar el xml con saltos de línea y tabuladores para facilitar la lectura
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//            for (Hermano hermano : hermanos) {
//            out.println(hermano.getNombre()+ "," + hermano.getApellidos()+ "(" + 
//            hermano.getDni()+ ")"); 
                entityManager.getTransaction().begin();
                switch (tipoPeticion) {
                    case PETICION_GET:
                        LOG.fine("Peticion Get");
                        for (Hermano hermano : hermanos.getListaHermanos()) {
                            // Añadir cada objeto a la lista general
                            LOG.fine("Lista hermanos[ nombre=" + hermano.getNombre()
                                    + "; apellidos=" + hermano.getApellidos() + " ]");
                        }
                        // Convertir a XML el contenido de hermanos generandolo en out
                        jaxbMarshaller.marshal(hermanos, out);
                        break;
                    case PETICION_POST:
                        // Obtener el hermano que se quiere insertar
                        Hermano hermanoNuevo = (Hermano) jaxbUnmarshaller.unmarshal(request.getInputStream());
                        entityManager.persist(hermanoNuevo);
                        LOG.fine("Peticion Put: Codigo postal=" + hermanoNuevo.getCodPostal() + " Nombre=" + hermanoNuevo.getNombre()
                                + " Dni=" + hermanoNuevo.getDni());
                        break;
                    case PETICION_PUT:
                        // Obtener el hermano que se quiere actualizar
                        Hermano hermanoEditado = (Hermano) jaxbUnmarshaller.unmarshal(request.getInputStream());
                        entityManager.merge(hermanoEditado);
                        LOG.fine("Peticion Put: Codigo postal=" + hermanoEditado.getCodPostal() + " Nombre=" + hermanoEditado.getNombre());
                        break;
                    case PETICION_DELETE:
                        Hermano hermanoEliminado = (Hermano) jaxbUnmarshaller.unmarshal(request.getInputStream());
                        //Buscamos el id del objeto hermano que queremos borrar en la base de datos
                        hermanoEliminado = entityManager.find(Hermano.class, hermanoEliminado.getId());
                        entityManager.remove(hermanoEliminado);
                        LOG.fine("Peticion Delete: Codigo postal=" + hermanoEliminado.getCodPostal() + " Nombre=" + hermanoEliminado.getNombre());
                        break;
                    default:
                        break;
                }
                entityManager.getTransaction().commit();

            } catch (JAXBException e) {
                out.println("<error>");
                e.printStackTrace(out);
                out.println("</error>");

            }
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_GET;
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_POST;

        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        tipoPeticion = PETICION_DELETE;
        try {
            processRequest(req, resp);
        } catch (JAXBException ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        tipoPeticion = PETICION_PUT;
        try {
            processRequest(req, resp);
        } catch (JAXBException ex) {
            Logger.getLogger(Servlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
