/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sergioTinoco.agenda;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1Daw02
 */
@Entity
@Table(name = "hermano")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hermano.findAll", query = "SELECT h FROM Hermano h"),
    @NamedQuery(name = "Hermano.findById", query = "SELECT h FROM Hermano h WHERE h.id = :id"),
    @NamedQuery(name = "Hermano.findByNombre", query = "SELECT h FROM Hermano h WHERE h.nombre = :nombre"),
    @NamedQuery(name = "Hermano.findByApellidos", query = "SELECT h FROM Hermano h WHERE h.apellidos = :apellidos"),
    @NamedQuery(name = "Hermano.findByEstadCivil", query = "SELECT h FROM Hermano h WHERE h.estadCivil = :estadCivil"),
    @NamedQuery(name = "Hermano.findByDni", query = "SELECT h FROM Hermano h WHERE h.dni = :dni"),
    @NamedQuery(name = "Hermano.findByLetraDni", query = "SELECT h FROM Hermano h WHERE h.letraDni = :letraDni"),
    @NamedQuery(name = "Hermano.findByNombreCalle", query = "SELECT h FROM Hermano h WHERE h.nombreCalle = :nombreCalle"),
    @NamedQuery(name = "Hermano.findByCodPostal", query = "SELECT h FROM Hermano h WHERE h.codPostal = :codPostal"),
    @NamedQuery(name = "Hermano.findByDomicilio", query = "SELECT h FROM Hermano h WHERE h.domicilio = :domicilio"),
    @NamedQuery(name = "Hermano.findByTelefono", query = "SELECT h FROM Hermano h WHERE h.telefono = :telefono"),
    @NamedQuery(name = "Hermano.findByProfesion", query = "SELECT h FROM Hermano h WHERE h.profesion = :profesion"),
    @NamedQuery(name = "Hermano.findByFechNacimiento", query = "SELECT h FROM Hermano h WHERE h.fechNacimiento = :fechNacimiento"),
    @NamedQuery(name = "Hermano.findByCiudadNacimento", query = "SELECT h FROM Hermano h WHERE h.ciudadNacimento = :ciudadNacimento"),
    @NamedQuery(name = "Hermano.findByProvinciaNacimiento", query = "SELECT h FROM Hermano h WHERE h.provinciaNacimiento = :provinciaNacimiento"),
    @NamedQuery(name = "Hermano.findByParroquia", query = "SELECT h FROM Hermano h WHERE h.parroquia = :parroquia"),
    @NamedQuery(name = "Hermano.findByCiudadParroquia", query = "SELECT h FROM Hermano h WHERE h.ciudadParroquia = :ciudadParroquia"),
    @NamedQuery(name = "Hermano.findByProvinciaParroquia", query = "SELECT h FROM Hermano h WHERE h.provinciaParroquia = :provinciaParroquia"),
    @NamedQuery(name = "Hermano.findByProvinciaHermano", query = "SELECT h FROM Hermano h WHERE h.provinciaHermano = :provinciaHermano"),
    @NamedQuery(name = "Hermano.findByNum", query = "SELECT h FROM Hermano h WHERE h.num = :num"),
    @NamedQuery(name = "Hermano.findByPiso", query = "SELECT h FROM Hermano h WHERE h.piso = :piso"),
    @NamedQuery(name = "Hermano.findByEmail", query = "SELECT h FROM Hermano h WHERE h.email = :email")})
public class Hermano implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellidos")
    private String apellidos;
    @Column(name = "estadCivil")
    private String estadCivil;
    @Column(name = "dni")
    private String dni;
    @Column(name = "letraDni")
    private String letraDni;
    @Column(name = "nombreCalle")
    private String nombreCalle;
    @Column(name = "codPostal")
    private String codPostal;
    @Column(name = "domicilio")
    private String domicilio;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "profesion")
    private String profesion;
    @Column(name = "fechNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechNacimiento;
    @Column(name = "ciudadNacimento")
    private String ciudadNacimento;
    @Column(name = "provinciaNacimiento")
    private String provinciaNacimiento;
    @Column(name = "parroquia")
    private String parroquia;
    @Column(name = "ciudadParroquia")
    private String ciudadParroquia;
    @Column(name = "provinciaParroquia")
    private String provinciaParroquia;
    @Column(name = "provinciaHermano")
    private String provinciaHermano;
    @Column(name = "num")
    private String num;
    @Column(name = "piso")
    private String piso;
    @Column(name = "email")
    private String email;

    public Hermano() {
    }

    public Hermano(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEstadCivil() {
        return estadCivil;
    }

    public void setEstadCivil(String estadCivil) {
        this.estadCivil = estadCivil;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getLetraDni() {
        return letraDni;
    }

    public void setLetraDni(String letraDni) {
        this.letraDni = letraDni;
    }

    public String getNombreCalle() {
        return nombreCalle;
    }

    public void setNombreCalle(String nombreCalle) {
        this.nombreCalle = nombreCalle;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public Date getFechNacimiento() {
        return fechNacimiento;
    }

    public void setFechNacimiento(Date fechNacimiento) {
        this.fechNacimiento = fechNacimiento;
    }

    public String getCiudadNacimento() {
        return ciudadNacimento;
    }

    public void setCiudadNacimento(String ciudadNacimento) {
        this.ciudadNacimento = ciudadNacimento;
    }

    public String getProvinciaNacimiento() {
        return provinciaNacimiento;
    }

    public void setProvinciaNacimiento(String provinciaNacimiento) {
        this.provinciaNacimiento = provinciaNacimiento;
    }

    public String getParroquia() {
        return parroquia;
    }

    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }

    public String getCiudadParroquia() {
        return ciudadParroquia;
    }

    public void setCiudadParroquia(String ciudadParroquia) {
        this.ciudadParroquia = ciudadParroquia;
    }

    public String getProvinciaParroquia() {
        return provinciaParroquia;
    }

    public void setProvinciaParroquia(String provinciaParroquia) {
        this.provinciaParroquia = provinciaParroquia;
    }

    public String getProvinciaHermano() {
        return provinciaHermano;
    }

    public void setProvinciaHermano(String provinciaHermano) {
        this.provinciaHermano = provinciaHermano;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getPiso() {
        return piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hermano)) {
            return false;
        }
        Hermano other = (Hermano) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sergioTinoco.agenda.Hermano[ id=" + id + " ]";
    }
    
}
